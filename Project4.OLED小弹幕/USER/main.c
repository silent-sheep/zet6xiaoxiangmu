#include "sys.h"
#include "delay.h"
#include "usart.h" 
#include "led.h" 		 	 
#include "lcd.h"  
#include "key.h"     
#include "usmart.h" 
#include "malloc.h"
#include "sdio_sdcard.h"  
#include "w25qxx.h"    
#include "ff.h"  
#include "exfuns.h"   
#include "text.h"
#include "i2c.h"
#include "oled0561.h"


 
 
/************************************************
 ALIENTEK精英STM32开发板实验37
 汉字显示 实验 
 技术支持：www.openedv.com
 淘宝店铺：http://eboard.taobao.com 
 关注微信公众平台微信号："正点原子"，免费获取STM32资料。
 广州市星翼电子科技有限公司  
 作者：正点原子 @ALIENTEK
************************************************/


 int main(void)
 {	    
	u16 base=32;
	u16 off=0; 
	delay_init();	    	 //延时函数初始化	  
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置中断优先级分组为组2：2位抢占优先级，2位响应优先级
	uart_init(115200);	 	//串口初始化为115200
 	usmart_dev.init(72);		//初始化USMART		
 	LED_Init();		  			//初始化与LED连接的硬件接口
	//KEY_Init();					//初始化按键
	// LCD_Init();			   		//初始化LCD   
	W25QXX_Init();				//初始化W25Q128
 	my_mem_init(SRAMIN);		//初始化内部内存池
	exfuns_init();				//为fatfs相关变量申请内存  
 	f_mount(fs[0],"0:",1); 		//挂载SD卡 
 	f_mount(fs[1],"1:",1); 		//挂载FLASH.
	font_init();
 	while(font_init()) 			//检查字库
 	{   
		OLED_Show_str(0,0,"字库损坏",0);
 		while(SD_Init())			//检测SD卡
 		{
			OLED_Show_str(2,0,"SD卡不存在",0);
 			delay_ms(200);
			OLED_DISPLAY_CLEAR();//清屏操作
 			delay_ms(200);		    
 		}								 						    
		OLED_Show_str(2,0,"SD卡准备好",0);
		delay_ms(2000);
 	}  
	I2C_Configuration();
	OLED0561_Init();
	OLED_DISPLAY_LIT(100);
	delay_ms(1000);	
	while(1)
	{	
		OLED_Show_str(2,base+off,"  sheep,你好呀",0);
		LED0=!LED0;
		delay_ms(200);
		off+=8;
		if((base+off)==65535)off=0;
	} 
}
















