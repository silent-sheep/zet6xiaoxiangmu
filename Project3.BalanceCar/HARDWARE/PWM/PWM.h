#ifndef __PWM_H__
#define __PWM_H__

#include "sys.h"

#define PWM_A_Ctrl(set_ccr) TIM_SetCompare1(TIM8,set_ccr)
#define PWM_AIN1 PAout(11)
#define PWM_AIN2 PAout(12)


void PWM_TIM8_Iint(u16 arr,u16 psc);


#endif
