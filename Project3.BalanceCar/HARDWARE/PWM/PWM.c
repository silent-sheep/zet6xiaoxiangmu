#include "PWM.h"

/*******************************************************************************
 @函 数 名         :void PWM_TIM8_Iint(u16 arr,u16 psc)
 @函数功能			 : 电机驱动的初始化 
 @输    入         : pwm,arr周期，psc预分频值
 @输    出         : 无
 @说    明         : PA11-AIN1;PA12-AIN2,⭐ stby引脚接VCC(后期可改为紧急制动)
*******************************************************************************/
void PWM_TIM8_Iint(u16 arr,u16 psc)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    TIM_OCInitTypeDef TIM_OCInitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC|RCC_APB2Periph_TIM8, ENABLE); //时钟使能
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE); //时钟使能

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC,&GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11|GPIO_Pin_12;//PA11/PA12作电机的方向控制引脚
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA,&GPIO_InitStructure);
    PWM_AIN1=1;
    PWM_AIN2=0;//默认以Ain1=1;正转

	TIM_TimeBaseStructure.TIM_Period = (arr-1); //设置在下一个更新事件装入活动的自动重装载寄存器周期的值	
	TIM_TimeBaseStructure.TIM_Prescaler = (psc-1); //设置用来作为TIMx时钟频率除数的预分频值
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;//不使用RCR计数
	TIM_TimeBaseInit(TIM8, &TIM_TimeBaseStructure); //根据指定的参数初始化TIMx的时间基数单位

    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;//<CCR有效
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;//有效电平为高
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;//OC1输出State使能
    TIM_OC1Init(TIM8,&TIM_OCInitStructure);
    TIM_CtrlPWMOutputs(TIM8,ENABLE);//开启TIM8的PWM通道，被BKR控制
    TIM_SetCompare1(TIM8,0);//开机默认将pwm设为0

    TIM_OC1PreloadConfig(TIM8,TIM_OCPreload_Enable);//使能OC1的CCR预装载
    TIM_ARRPreloadConfig(TIM8,ENABLE);//使能TIM8的ARR预装载
	TIM_Cmd(TIM8, ENABLE);  //使能TIMx	

}

// void PWM_TIM8_Ctrl(u16 SET_CCR)
// {
//     TIM_SetCompare1(TIM8,SET_CCR);
// }
//原本用函数封装，改为宏替代
