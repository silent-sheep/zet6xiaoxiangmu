#include "Controler.h"

/*******************************************************************************
 函 数 名         : void Controler_Init(void)
 函数功能			 : 整个pid速度环的控制
 输    入         : 无
 输    出         : 无
 说    明         : 注意各个参数及宏查看头文件
*******************************************************************************/
void Controler_Init(void)
{
    PWM_TIM8_Iint(PWM_Arr,PWM_Psc);//电机驱动初始化
    Encoder_Init(Encoder_Psc);//编码器初始化，5分频；
    Controler_TIM_Init();//编码器测速及pid周期调用初始化；
    PID_param_init(0.5,0.2,0.0,180,is_IncreasementPID);//初始化参数
}


/*******************************************************************************
 函 数 名         : void Controler_TIM_Init(void)
 函数功能			 : 控制器的定时调用初始化
 输    入         : 无
 输    出         : 无
 说    明         : 注意各个参数及宏查看头文件，基本定时器只有两个时基参数
*******************************************************************************/
void Controler_TIM_Init(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);

    TIM_TimeBaseStructure.TIM_Period =Controler_Arr-1; //设置为最大值65535避免溢出
	TIM_TimeBaseStructure.TIM_Prescaler = Controler_Psc-1; //设置用来作为TIMx时钟频率除数的预分频值
	TIM_TimeBaseInit(TIM6, &TIM_TimeBaseStructure); 

    NVIC_InitStructure.NVIC_IRQChannel=TIM6_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=2;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority=1;
    NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
    NVIC_Init(&NVIC_InitStructure);
    TIM_ClearFlag(TIM6,TIM_FLAG_Update);//使能前先清除标志位
    TIM_ITConfig(TIM6,TIM_IT_Update,ENABLE);//使能溢出标志

    TIM_Cmd(TIM6,ENABLE);
}



/*******************************************************************************
 函 数 名         :void TIM6_IRQHandler(void)
 函数功能			 : 控制器的定时器中断函数
 输    入         : 无
 输    出         : 无
 说    明         : 无
*******************************************************************************/
void TIM6_IRQHandler(void)
{
    KeyValue temp_keyvalue;//用于输出的结构体
    static float actual_speed=0;
    static _pid temp_pid;//临时pid结构体用于取出pid值
    static float Pid_result=0;
    if (TIM_GetITStatus(TIM6,TIM_IT_Update))
    {
        actual_speed = 60*1000*Encoder_Read()/(Encoder_C*Encoder_T0);//转每分钟
        Pid_result = Increasement_PID_realize(actual_speed);//增量式pid的调用
        //返回数据给上位机
        if(Get_PID_Target_P_I_D(&temp_pid,is_IncreasementPID))
        {
            printf("speedA-T-pwm-p-i-d:%f,%f,%f,%f,%f,%f,%d\n"
            ,actual_speed , temp_pid.target_val , Pid_result,temp_pid.Kp , temp_pid.Ki , temp_pid.Kd , Battery_Volt);//将速度及参数，电压值返回给上位机
        }
        //pid输出控制
        if(Pid_result<0)
        {
            PWM_AIN1=0;PWM_AIN2=1;//反转
            Pid_result=-Pid_result;
        }
        else {PWM_AIN1=1;PWM_AIN2=0;}//正转
        if (Pid_result>PWM_Arr)Pid_result=PWM_Arr;//输出限幅PWM_ARR
        PWM_A_Ctrl(Pid_result);//输入电机pwm
/*调试软件pid时使用的*/
        // Pid_result =PID_realize(actual_speed);
        // actual_speed = Pid_result;
        // printf("PIDA-T:%f,%f\n",actual_speed,target_speed);
/*******************/
//以下是接收参数的分析
        if(Receive_firewater(&temp_keyvalue))
		{
			switch (temp_keyvalue.Key)
			{
			case 't': 	set_pid_target(temp_keyvalue.Value,is_IncreasementPID);		break;//target数据
			case 'p': 	set_p(temp_keyvalue.Value,is_IncreasementPID);		break;//p参数
			case 'i': 	set_i(temp_keyvalue.Value,is_IncreasementPID);		break;//i参数
			case 'd': 	set_d(temp_keyvalue.Value,is_IncreasementPID);		break;//d参数
            case 's':   if(temp_keyvalue.Value>=0){PWM_AIN1=1;PWM_AIN2=0;PWM_A_Ctrl(temp_keyvalue.Value); break;}
                        else { PWM_AIN1=0;PWM_AIN2=1;PWM_A_Ctrl(-1*temp_keyvalue.Value);break;}
                        //直接操作PWM的s(speed)参数
            case 'r':   NVIC_SystemReset();break;//重启系统
			// default: printf("参数设置错误\n");
			// 	break;
			}
		}
        TIM_ClearITPendingBit(TIM6,TIM_IT_Update);//清除溢出标志
    }
}
