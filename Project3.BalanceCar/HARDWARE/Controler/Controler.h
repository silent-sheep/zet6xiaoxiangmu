#ifndef __Controler_H__
#define __Controler_H__

#include "sys.h"
#include "PWM.h"
#include "pid.h"
#include "FireWater.h"
#include "Encoder.h"
#include "Power_ADC.h"

/*有关检测编码器速度的时间周期，及pid调用的时间周期宏定义*/
#define Controler_Arr 100  //间隔10ms检测
#define Controler_Psc 7200

/*有关编码器参数的宏定义*/
#define Encoder_C 12000    //(Encoder_PPR*Encoder_GR*Encoder_P/Encoder_Psc)这里直接给出，也可用宏计算 
#define Encoder_PRR 500  //线数
#define Encoder_GR 30   //减速比
#define Encoder_P 4     //四倍频检测
#define Encoder_Psc 5     //5分频
#define Encoder_T0 10     //10ms

/*有关电机PWM驱动的宏定义*/
#define PWM_Arr 1000     //
#define PWM_Psc 7199     //PWM周期0.1s


void Controler_TIM_Init(void);
void Controler_Init(void);

#endif

/*接线说明汇总
编码器：A相PA0,B相PA1
电机： PA11-AIN1;PA12-AIN2,⭐ stby引脚接VCC(后期可改为紧急制动)
电压读取：VADC,PA5,共地
*/






