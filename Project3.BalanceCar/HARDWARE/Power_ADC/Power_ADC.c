#include "Power_ADC.h"



/*******************************************************************************
 @函 数 名         : void  Baterry_Adc_Init(void)
 @函数功能			 :电压测量初始化
 @输    入         : 无
 @输    出         : 无
 @说    明         : 注意时钟分频，ADC时钟不能超过14Mhz
*******************************************************************************/

void  Baterry_Adc_Init(void)
{  
	GPIO_InitTypeDef GPIO_InitStructure;
    ADC_InitTypeDef ADC_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE ); //使能GPIOA时钟
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;        //PA5
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;		//模拟输入
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1,ENABLE );  //使能ADC1时钟
    RCC_ADCCLKConfig(RCC_PCLK2_Div6);//6分频，12Mhz
    ADC_DeInit(ADC1);        //ADC1复位
	
 	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;	//ADC工作模式:独立模式
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;	//单通道模式
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;	//单次转换模式
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;	//转换由软件而不是外部触发启动
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;	//ADC数据右对齐
	ADC_InitStructure.ADC_NbrOfChannel = 1;	//顺序进行规则转换的ADC通道的数目
	ADC_Init(ADC1, &ADC_InitStructure);	//初始化外设ADCx的寄存器   

  
	ADC_Cmd(ADC1, ENABLE);	//使能指定的ADC1
	
	ADC_ResetCalibration(ADC1);	//使能复位校准  
	while(ADC_GetResetCalibrationStatus(ADC1));	//等待复位校准结束
	ADC_StartCalibration(ADC1);	 //开启AD校准
	while(ADC_GetCalibrationStatus(ADC1));	 //等待校准结束
}		

/**************************************************************************
函数功能：AD采样
入口参数：ADC1 的通道
返回  值：AD转换结果
作    者：平衡小车之家
**************************************************************************/
u16 Get_Adc(u8 ch)   
{
	
	//设置指定ADC的规则组通道，一个序列，采样时间
	ADC_RegularChannelConfig(ADC1, ch, 1, ADC_SampleTime_239Cycles5 );	//ADC1,ADC通道,采样时间为239.5周期	  			    
  
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);		//使能指定的ADC1的软件转换启动功能	
	 
	while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC ));//等待转换结束

	return ADC_GetConversionValue(ADC1);	//返回最近一次ADC1规则组的转换结果
	
}

/**************************************************************************
函数功能：读取电池电压 
入口参数：无
返回  值：电池电压 单位MV
作    者：平衡小车之家
**************************************************************************/
int Get_battery_volt(void)   
{  
	int Volt;                                          //电池电压
	Volt=Get_Adc(Battery_Ch)*3.3*11*100/1.0/4096;	//12位ADC,板载电压缩小了11倍；	
	return Volt;
}

/***************************************************************************
由于电源电压测算有失误，下面的电压值换算待改正，因为7.4V的电压测出为900+,共地400+
***************************************************************************/

/*******************************************************************************
 函 数 名         : u8  Battery_calulation(u8 type_battery)
 函数功能			 :显示转换后的电压百分比 
 输    入         : 电池的类型  12☞11.1V / 8☞7.4V
 输    出         : 转换后的百分比可用于指示电量是否过低
 说    明         : 无
*******************************************************************************/
u8  Battery_calulation(u8 type_battery)
{
    u8 Volt = 0,result=0 , charge_zoom=0;//电池增量区间
    Volt = Get_battery_volt();
    if (type_battery==12)
    {
        charge_zoom=1260-1110;//12V电池为11.1-12.6V
        result=(Volt -1110)/charge_zoom;
    }
    else if(type_battery==8) 
    {
        charge_zoom=860-740;//8V电池为7.4-8.6V
        result=(Volt -740)/charge_zoom;
    }
    return result;
}

