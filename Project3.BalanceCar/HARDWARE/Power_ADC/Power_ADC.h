#ifndef __Power_ADC_H__
#define __Power_ADC_H__

#include "sys.h"

extern int Battery_Volt;//外部变量用于调出电压值
#define Battery_Ch ADC_Channel_5//电压检测adc1ch5

void  Baterry_Adc_Init(void);
u16 Get_Adc(u8 ch);
int Get_battery_volt(void) ;
u8  Battery_calulation(u8 type_battery);

#endif
