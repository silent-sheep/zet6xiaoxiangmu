#include "Encoder.h"

#if __Overflow_Check__
 static int Encoder_OverFlow;
#endif


/*******************************************************************************
 @函 数 名         : void Encoder_Init(u16 Encoder_psc)
 @函数功能			 : 编码器的初始化
 @输    入         : 编码器的psc,需要后期拿来计算速度的数值，由于没有溢出检测，建议分频值不小于5
 @输    出         : 无
 @说    明         : 部分宏是关于溢出检测的
*******************************************************************************/
void Encoder_Init(u16 Encoder_psc)
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    TIM_ICInitTypeDef TIM_ICInitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;

#if __Overflow_Check__
    NVIC_InitTypeDef NVIC_InitStructure;
#endif 

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA,&GPIO_InitStructure);

#if __Overflow_Check__
    TIM_TimeBaseStructure.TIM_Period = Encoder_ARR; //设置为最大值60000避免溢出
#else
    TIM_TimeBaseStructure.TIM_Period = 65535; //设置为最大值65535避免溢出
#endif

	TIM_TimeBaseStructure.TIM_Prescaler = Encoder_psc-1; //设置用来作为TIMx时钟频率除数的预分频值
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure); 

#if __Overflow_Check__
    NVIC_InitStructure.NVIC_IRQChannel=TIM2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=2;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority=2;
    NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
    NVIC_Init(&NVIC_InitStructure);
    TIM_ClearFlag(TIM2,TIM_FLAG_Update);//使能前先清除标志位
    TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);//使能溢出标志
#endif 

    TIM_ICStructInit(&TIM_ICInitStructure);//默认赋值
    TIM_EncoderInterfaceConfig(TIM2,TIM_EncoderMode_TI12,TIM_ICPolarity_Rising,TIM_ICPolarity_Rising);
    //四倍通道输入
    TIM_ICInit(TIM2,&TIM_ICInitStructure);//将默认值填入

    TIM_SetCounter(TIM2,0);
    TIM_Cmd(TIM2,ENABLE);

}



#if (!__Overflow_Check__)
/*******************************************************************************
 @函 数 名         : int Encoder_Read(void)
 @函数功能			 : 读取编码器捕捉到的脉冲值
 @输    入         : 无
 @输    出         : 整形，脉冲数
 @说    明         : 将u16强转成short,在向下计数时，利用补码的关系可以在向下计数成返回成计数的负整数
*******************************************************************************/
int Encoder_Read(void)
{
    int result=0;
    result = (short)TIM2->CNT;
    TIM2->CNT = 0;
    return result ;
}
#endif



/**********************************************/
//以下为Encoder_PSC过小时的对CNT溢出的操作处理，实测发现数值在负数时波动太大，以弃置
#if __Overflow_Check__

int Encoder_Read(void)
{
    long result=0;
    u16 Encoder_CNT;    
    Encoder_CNT= TIM2 -> CNT;//读出除周期的剩下的CNT
    TIM2 -> CNT=0;//读完后清零，方便下次读
    if((Encoder_CNT==0)&&(Encoder_OverFlow==0))result=0;
    else if(TIM2->CR1&TIM_CR1_DIR)result = (Encoder_OverFlow-1)*Encoder_ARR+Encoder_CNT;
    else result = Encoder_OverFlow*Encoder_ARR+Encoder_CNT;//将整周期的值加上
    Encoder_OverFlow=0;//读完后清零，方便下次读
	return result;
}

void TIM2_IRQHandler(void)
{
    if (TIM_GetITStatus(TIM2,TIM_IT_Update))
    {
        if(TIM2->CR1&TIM_CR1_DIR)Encoder_OverFlow--;
        else Encoder_OverFlow++;
        TIM_ClearITPendingBit(TIM2,TIM_IT_Update);
    }
}

#endif
