#ifndef __Encoder_H__
#define __Encoder_H__

#include "sys.h"
#include "usart.h"

#define __Overflow_Check__ 0

#define Encoder_ARR 60000


void Encoder_Init(u16 Encoder_Psc);
int Encoder_Read(void);


#endif
