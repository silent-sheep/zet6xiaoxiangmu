#ifndef __PID_H__
#define __PID_H__

#include "sys.h"
#include <stdio.h>
#include <stdlib.h>

#define is_PositionPID 1
#define is_IncreasementPID 0
#define PID_Limition 20

/*pid*/
typedef struct
{
    float target_val;           //目标值
    float actual_val;        		//实际值
    float err;             			//定义偏差值
    float err_last;          		//定义上一个偏差值
    float err_last_last;            //定义上上一次偏差值
    float Kp,Ki,Kd;          		//定义比例、积分、微分系数
    float integral;          		//定义积分值（）用于记录累加值）
}_pid;

void PID_param_init(float default_Kp,float default_Ki,float default_Kd,float deflaut_target,u8 posORinc);
void set_pid_target(float temp_val,u8 posORinc);
void set_p(float p,u8 posORinc);
void set_i(float i,u8 posORinc);
void set_d(float d,u8 posORinc);
u8 Get_PID_Target_P_I_D(_pid* temp_pid,u8 posORinc);
float Position_PID_realize(float temp_val);
float Increasement_PID_realize(float temp_val) ;


#endif
