#include "FireWater.h"

/*******************************************************************************
 @函 数 名         : u16  Receive_firewater(KeyValue* tempKeyValue)
 @函数功能			 : 用于获取fireWater协议发送来的键值对
 @输    入         : 传入一keyvalue结构体的地址
 @输    出         : u6 Is_Success，成功为1；否则为0
 @说    明         : 一次获取一个键值对的值，传入的结构体 会获得值
*******************************************************************************/

u16  Receive_firewater(KeyValue* tempKeyValue)
{
	float_union temp;
	u16 i = 0, Is_Success = 1;//读取成功标志
	u16 length;
	if (USART_RX_STA&0xC000)//接收到数据
	{	
		length = (USART_RX_STA&0x3FFF);//取出数据长度
		if ( (length != 6) || (USART_RX_BUF[1] != ':') )Is_Success = 0;
         //判断是否符合所要求的键值对格式 u8:float
		else
		{
			tempKeyValue->Key = USART_RX_BUF[0];//取出键key
			for(i=2;i<6;i++)
			{
				temp.cv[i-2] = USART_RX_BUF[i];
			}//转化为float
			tempKeyValue->Value = temp.fv;//取出值（float赋值）
		} 
		USART_RX_STA=0;//清除接收标志
	}
	return Is_Success;
}


/*
    KeyValue p_keyvalue;
	        if(Receive_firewater(&p_keyvalue))
		{
			switch (p_keyvalue.Key)
			{
			case 'p': 	set_p(p_keyvalue.Value);		break;
			case 'i': 	set_i(p_keyvalue.Value);		break;
			case 'd': 	set_d(p_keyvalue.Value);		break;
			case 't': 	set_pid_target(p_keyvalue.Value);		break;
			// default: printf("设置错误\n");
			// 	break;
			}
						// printf("Key-value-encoder-Volt:%c,%f,%d,%d,%d\n",p_keyvalue.Key,p_keyvalue.Value,speed,Battery_Volt,Get_ADC_Value);
			// PWM_1_Ctrl(p_keyvalue.Value);
		}




*/


