#ifndef __FireWater_H__
#define __FireWater_H__

#include "sys.h"
#include "usart.h"

typedef union{
	float fv;
	u8 cv[4];
}float_union;
//共用体，用于将上位机传进来的小端float的十六进制形式，改成float读出


typedef struct _Key_Value
{
	u8 Key;
	float Value;
}KeyValue;
//上位机应使用如json中键值对的形式将数据发送 --key:value\r\n(换行为结束符)
//其中Key为单字符；Value为float

u16  Receive_firewater(KeyValue* tempKeyValue);


#endif

