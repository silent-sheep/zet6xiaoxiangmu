#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "lcd.h"
#include "usart.h"	 
#include "usmart.h"	
#include "rtc.h"	
#include <math.h>

#define Pi 3.1415926
#define Center_Ponitx 160
#define Center_Ponity 240
#define Second 0
#define Minute 1
#define Hour   2
typedef struct clock_position
{
	u16 x[60];
	u16 y[60];
} clock_pos;
//用于存放时分秒针针尾像素位置的结构体
void Clock_PinInit(int Sta12Posx,int Sta12Posy,char type);//时针结构体像素坐标的初始化
void Clock_NumInit(void);//绘制12个数字

clock_pos clock_pin_pos[3];
clock_pos clock_mun_pos;

int Circle_R[3]={120,90,60};

 int main(void)
 {	 
	signed char i[3] = {0};
	delay_init();	    	 //延时函数初始化	  
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2); 	 //设置NVIC中断分组2:2位抢占优先级，2位响应优先级
	uart_init(115200);	 	//串口初始化为115200
	LCD_Init();
	RTC_Init();//实时时钟初始化
	usmart_dev.init(SystemCoreClock/1000000);	//初始化USMART	
	Clock_PinInit(160,120,Second);//秒针坐标初始化
	Clock_PinInit(160,150,Minute);//分针坐标初始化
	Clock_PinInit(160,180,Hour);//分针坐标初始化
	 
	POINT_COLOR=RED;
	BACK_COLOR = CYAN;
	LCD_Clear(BACK_COLOR);//设置背景色，个人喜欢这个颜色；
	LCD_ShowString(0,0,60,24,24,"Sheep");//署名标签 ^_^
	LCD_Draw_Circle(160,240,150);//画圆，时钟边框；
	Clock_NumInit();//绘制12个数字；
	LCD_ShowString(90,410,200,24,24,"    -  -  ");	//绘制日期间隔符号
	LCD_ShowString(100,440,200,24,24,"  :  :  ");	//绘制时间间隔符号
	i[Second] = calendar.sec;//秒针位置初始化
	i[Minute] = calendar.min;//分针位置初始化
	if(calendar.hour >= 12)
	{
		i[Hour] = (calendar.hour-12)*5+calendar.min/60.0*5;
	}
	else i[Hour] = (calendar.hour*5)+calendar.min/60.0*5;
	//时针位置初始化

  	while(1) 
	{
		LCD_ShowNum(90,410,calendar.w_year,4,24);
		LCD_ShowxNum(150,410,calendar.w_month,2,24,0);
		LCD_ShowNum(186,410,calendar.w_date,2,24);
		LCD_ShowxNum(100,440,calendar.hour,2,24,0);
		LCD_ShowxNum(136,440,calendar.min,2,24,0);
		LCD_ShowxNum(172,440,calendar.sec,2,24,0);
		//数字时钟及日期显示

		i[Second]++;
		POINT_COLOR = CYAN;//覆盖为背景色划线		
		LCD_DrawLine(Center_Ponitx,Center_Ponity,clock_pin_pos[Second].x[ i[Second]-1 ],clock_pin_pos[Second].y[ i[Second]-1 ] );
		if(i[Second]==60)
		{
			i[Minute]++; 
			LCD_DrawLine(Center_Ponitx,Center_Ponity,clock_pin_pos[Minute].x[ i[Minute]-1 ],clock_pin_pos[Minute].y[ i[Minute]-1 ] );
			if(i[Minute] == 60)
			{
				i[Hour]++;
				LCD_DrawLine(Center_Ponitx,Center_Ponity,clock_pin_pos[Hour].x[ i[Hour]-1 ],clock_pin_pos[Hour].y[ i[Hour]-1 ] );
				i[Hour]%=60;
			}
			i[Minute]%=60;
		}
		i[Second]%=60;
		//擦除（覆盖）上一次的时针

		POINT_COLOR = RED;//秒针红色		
		LCD_DrawLine(Center_Ponitx,Center_Ponity,clock_pin_pos[Second].x[ i[Second] ],clock_pin_pos[Second].y[ i[Second] ]);	
		POINT_COLOR = BLUE;//分针蓝色		
		LCD_DrawLine(Center_Ponitx,Center_Ponity,clock_pin_pos[Minute].x[ i[Minute] ],clock_pin_pos[Minute].y[ i[Minute] ]);
		POINT_COLOR = BLACK;//时针黑色		
		LCD_DrawLine(Center_Ponitx,Center_Ponity,clock_pin_pos[Hour].x[ i[Hour] ],clock_pin_pos[Hour].y[ i[Hour] ]);
		//画上此次的时针

		LED0=!LED0;					 
		delay_ms(1000);	//一秒钟刷新一次页面
	}			
 }

/*
@ 函数名	：void Clock_PinInit(int Sta12Posx,int Sta12Posy,char type)
@ 输入参数	：Start2Posx,Start2Posy,起始的基准坐标（12点钟针末尾坐标），type：用于决定存放于那个结构体（Second/Minute,Hour）
@ 输出		：无
@ 说明		：初始化调用，确定各指针尾的坐标，因为涉及sin，cos,运算时间较长，故在初始化时就将各位置算好存在数组结构体中
*/
void Clock_PinInit(int Sta12Posx,int Sta12Posy,char type)
{
	u8 i = 0;
	for ( i = 0; i < 60; i++)
	{
		clock_pin_pos[type].x[i] = Sta12Posx + Circle_R[type]*sin(Pi*i/30);
		clock_pin_pos[type].y[i] = Sta12Posy + Circle_R[type]*( 1- cos(Pi*i/30) ); 
	}
}

/*
@ 函数名	：void Clock_NumInit(void)
@ 输入参数	：无
@ 输出		：无
@ 说明		：用于绘制12个时钟数字在TFTLCD屏（3.5英寸）上
*/

void Clock_NumInit(void)
{
	u8 i = 1;
	clock_mun_pos.x[1] = clock_mun_pos.x[5] = 228;
	clock_mun_pos.y[1] = clock_mun_pos.y[11] = 108;
	clock_mun_pos.x[2] = clock_mun_pos.x[4] = 275;
	clock_mun_pos.y[2] = clock_mun_pos.y[10] = 160;
	clock_mun_pos.x[3] = 300;
	clock_mun_pos.y[3] = clock_mun_pos.y[9] = 232;
	clock_mun_pos.y[4] = clock_mun_pos.y[8] = 304;
	clock_mun_pos.y[5] = clock_mun_pos.y[7] = 356;
	clock_mun_pos.x[6] = 156;
	clock_mun_pos.y[6] = 374;
	clock_mun_pos.x[7] = clock_mun_pos.x[11] = 94;
	clock_mun_pos.x[8] = clock_mun_pos.x[10] = 37;
	clock_mun_pos.x[9] = 12;
	clock_mun_pos.x[12] = 152;
	clock_mun_pos.y[12] = 92;
//以上纯属手工测绘得到的数值，原以为12个数不会浪费太多时间的，……说多了都是眼泪/(ㄒoㄒ)/~~
	for(i = 1; i < 13 ;i ++)
	{
		if(i<10)
		LCD_ShowNum(clock_mun_pos.x[i],clock_mun_pos.y[i],i,1,16);
		else
		LCD_ShowNum(clock_mun_pos.x[i],clock_mun_pos.y[i],i,2,16);
	}

}
